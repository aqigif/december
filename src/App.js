import "./App.css";
import Button from "./components/Button/button";

function App() {
  return (
    <div className="App">
      <img
        src="/doc.jpeg"
        alt="img"
        style={{ maxHeight: 400, borderRadius: 12 }}
      />
      <Button label="React JS CI/CD" />
      <p style={{ fontSize: 12, fontWeight: "bold", color: "#E5E5E5" }}>
        23 December 2020
      </p>
    </div>
  );
}

export default App;
