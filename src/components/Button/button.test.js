import ReactDOM from 'react-dom'
import { render, screen } from '@testing-library/react';
import Button from './button';

test('renders button', () => {
  const div = document.createElement("div")
  ReactDOM.render(<Button />, div)
});

test('button render', () => {
  const { getByTestId } = render(<Button label={"test"} />)
  expect(getByTestId('button')).toHaveTextContent("test")
});